**# SPARSE SEARCH CONSOLE #**

Version : Sparse Search 1.0

Author : [primayudantra](http://twitter.com/_prmydntr) // Miftah Muslim // Yehezkiel Wijaya
### How to run it? ###

```
#!Run
$ git clone https://bitbucket.org/primayudantra/sparse-search-fullstack
$ cd /path/sparse-search-fullstack
$ npm install
$ bower install
$ node app / node-dev app


Welcome to the world :)
```


### If you have a question.. feel free to contact me! ###

* Email : prima.yudantra@gmail.com / prima.yudantra@mediatrac.co
* Phone : +6281294919126 / +6287861546526