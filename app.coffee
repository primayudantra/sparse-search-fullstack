/* 
	Author : Prima Yudantra
	Desc	 : This app for frontend sparse search
	Last Update : May 20th, 2016
*/

express = require 'express'
async 	= require 'async'
_ 		= require 'underscore'
app		= express()
	
ejs 	= require('ais-ejs-mate')({open:'<%', close:'%>'})

### ***************************
# 		EXPRESS SETUP
# *************************** ###
app.engine '.html', ejs
app.use(express.static(__dirname + '/public'));
app.set 'view engine', 'html'


### ***************************
# 		ROUTING EXAMPLE
# *************************** ###
app.get '/api/v1', (req, res) ->
	res.send 'welcome to API'


### ***************************
#		REFRESH SCRIPT
# *************************** ###
app.get '*', (req,res) ->
	res.sendfile './public/index.html'


	
### ***************************
# 		SERVER SETUP
# *************************** ###
port = 8888

app.listen port, ()->
	console.log ""
	console.log "==============================="
	console.log "Welcome to the SPARSE Search"
	console.log "Server running on localhost:" + port
	console.log "Welcome to the world :)"
	console.log "==============================="