'use strict';

angular
	.module('appRoutes', [])
		.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
			$routeProvider

			.when('/', {
				templateUrl: '../views/templates/test-pages.html',
				controller:'TestCtrl'
			})
			.when('/ranking',{
				templateUrl: '../views/templates/customize-ranking.html',
				controller:'RankingCtrl'	
			})
			.when('/weighting',{
				templateUrl: '../views/templates/customize-weighting.html',
				controller:'WeightingCtrl'	
			})
			.when('/synonyms',{
				templateUrl: '../views/templates/customize-synonyms.html',
				controller:'SynonymsCtrl'	
			});
		$locationProvider.html5Mode(true);
	}]);