'use strict';

angular
	.module('searchApp')
		.directive('menu', function(){
			return{
				templateUrl: 'views/partials/menu.html',
				restrict:'E'
			}
	})